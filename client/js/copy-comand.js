/**
 * Created by barnsAdmin on 24/10/2016.
 */
function copycommand() {
    var commandText = document.querySelector('input[name=chmod_command]');
    commandText.select();

    try {
        var successful = document.execCommand('copy');
        var msg = successful ? 'successful' : 'unsuccessful';
        console.log('Copying text command was ' + msg);
    } catch (err) {
        console.log('Oops, unable to copy');
    }
}
